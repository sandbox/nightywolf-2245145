<?php

/**
 * Menu callback; Show list of email types we can add.
 */
function emails_add_page() {
  $item = menu_get_item();
  $links = system_admin_menu_block($item);

  foreach ($links as $link) {
    $items[] = l($link['title'], $link['href'], $item['localized_options'])
      . ': ' . filter_xss_admin($link['description']);
  }

  return theme('item_list', array('items' => $items));
}

/**
 * Present an email submission form.
 */
function emails_add($type) {
  global $user;

  $types = emails_types();
  $type = isset($type) ? str_replace('-', '_', $type) : NULL;
  if (empty($types[$type])) {
    return MENU_NOT_FOUND;
  }

  $email = entity_get_controller('email')->create($type);

  drupal_set_title(t('Create @name', array('@name' => $types[$type]->name)), PASS_THROUGH);
  return drupal_get_form($type . '_emails_form', $email);
}

/**
 * Menu callback; presents the email editing form, or redirects to delete confirmation.
 *
 * @param $email
 *   The email object to edit.
 */
function emails_page_edit($email) {
  $types = emails_types();
  drupal_set_title(t('<em>Edit @type</em> @title', array('@type' => $types[$email->type]->name, '@title' => $email->title)), PASS_THROUGH);

  return drupal_get_form($email->type . '_emails_form', $email);
}

/**
 * Form builder; Displays the email add/edit form.
 *
 * @param $form
 * @param $form_state
 * @param $email
 *   The email object to edit, which may be brand new.
 */
function emails_form($form, &$form_state, $email) {

  // Set the id and identify this as an email edit form.
  $form['#id'] = 'emails-form';

  // Save the email for later, in case we need it.
  $form['#email'] = $email;
  $form_state['email'] = $email;

  // Common fields. We don't have many.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $email->title,
    '#weight' => -5,
    '#required' => TRUE,
  );

  $form['revision'] = array(
    '#access' => user_access('administer emails'),
    '#type' => 'checkbox',
    '#title' => t('Create new revision'),
    '#default_value' => 0,
  );

  // Add the buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('emails_form_submit'),
  );
  if (!empty($email->eid)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete emails'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('emails_form_delete_submit'),
    );
  }

  $form['#validate'][] = 'emails_form_validate';

  field_attach_form('email', $email, $form, $form_state);

  return $form;
}

function emails_form_validate($form, &$form_state) {
  $email = $form_state['email'];

  // Field validation.
  field_attach_form_validate('email', $email, $form, $form_state);
}

function emails_form_submit($form, &$form_state) {
  global $user;

  $email = &$form_state['email'];

  // Set the email's uid if it's being created at this time.
  if (empty($email->uid)) {
    $email->uid = $user->uid;
  }

  $email->title = $form_state['values']['title'];
  $email->revision = $form_state['values']['revision'];

  // Notify field widgets.
  field_attach_submit('email', $email, $form, $form_state);

  // Save the email.
  emails_save($email);

  // Notify the user.
  drupal_set_message(t('Email saved.'));

  $form_state['redirect'] = 'admin/structure/emails/' . $email->eid;
}

function emails_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $email = $form['#email'];
  $form_state['redirect'] = array('admin/structure/emails/' . $email->eid . '/delete', array('query' => $destination));
}

/**
 * Displays an email.
 *
 * @param $email
 *   The email object to display.
 * @param $view_mode
 *   The view mode we want to display.
 */
function emails_page_view($email, $view_mode = 'full') {
  // Remove previously built content, if exists.
  $email->content = array();

  if ($view_mode == 'teaser') {
    $email->content['title'] = array(
      '#markup' => filter_xss($email->title),
      '#weight' => -5,
    );
  }

  // Build fields content.
  field_attach_prepare_view('email', array($email->eid => $email), $view_mode);
  entity_prepare_view('email', array($email->eid => $email));
  $email->content += field_attach_view('email', $email, $view_mode);

  return $email->content;
}

/**
 * Form bulder; Asks for confirmation of email deletion.
 */
function emails_delete_confirm($form, &$form_state, $email) {
  $form['#email'] = $email;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['eid'] = array('#type' => 'value', '#value' => $email->eid);
  return confirm_form($form, t('Are you sure you want to delete %title?', array('%title' => $email->title)), 'email/' . $email->eid, t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Executes email deletion.
 */
function emails_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $email = emails_load($form_state['values']['eid']);
    emails_delete($form_state['values']['eid']);
    watchdog('email', '@type: deleted %title.', array('@type' => $email->type, '%title' => $email->title));

    $types = emails_types();
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $types[$email->type]->name, '%title' => $email->title)));
  }

  $form_state['redirect'] = '<front>';
}

/**
 * Menu callback; Displays a listing of recent emails.
 *
 * This doesn't really work yet because our presentation code doesn't show
 * the title.
 */
function emails_page_list_recent() {
  $content = array();

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'email')
    ->propertyOrderBy('created', 'DESC')
    ->range(0, 5);
  $result = $query->execute();

  $emails = emails_load_multiple(array_keys($result['email']));
  foreach ($emails as $email) {
    $content[$email->eid] = emails_page_view($email, 'teaser');
  }

  return $content;
}
