<?php

/**
 * Menu callback; List all email types available.
 */
function emails_overview_types() {
  foreach (emails_types() as $type => $info) {
    $type_url_str = str_replace('_', '-', $type);
    $label = t('View @type', array('@type' => $info->name));
    $items[] = l($label, 'admin/structure/emails/manage/' . $type_url_str);
  }

  return theme('item_list', array('items' => $items));
}

/**
 * Menu callback; Email information page.
 *
 * @param object $email_type
 */
function emails_information($email_type) {
  return $email_type->name . ': ' . $email_type->description;
}
