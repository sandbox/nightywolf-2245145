This is a small module that add a very useful feature for menus blocks created 
with the Menu block module. You can hide some selected links when the block is 
rendered. 

# How to use : 1- Install Menu block module first. 2- Install Menu block ignored 
links 3- Create a menu block 4- You will see in the block configuration a new 
select field called Ignored menus 5- Choose the links to hide, (of course the 
links must be part of the chosen menu) 6- Save, and appreciate the magic! 
