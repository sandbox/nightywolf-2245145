<?php

/**
 * Controller for loading, creating, and saving emails.
 *
 * The default loader, which we extend, handles load() already. We only
 * need to add saving and creating.
 */
class EmailsController extends DrupalDefaultEntityController {

  public function save($email) {
    $transaction = db_transaction();

    try {
      global $user;

      // Determine if we will be inserting a new email.
      $email->is_new = empty($email->eid);

      // Set the timestamp fields.
      if (empty($email->created)) {
        $email->created = REQUEST_TIME;
      }
      $email->changed = REQUEST_TIME;

      $email->revision_timestamp = REQUEST_TIME;
      $update_email = TRUE;

      // Give modules the opportunity to prepare field data for saving.
      field_attach_presave('email', $email);

      // When saving a new email revision, unset any existing $email->vid
      // to ensure a new revision will actually be created and store the old
      // revision ID in a separate property for email hook implementations.
      if (!$email->is_new && !empty($email->revision) && $email->vid) {
        $email->old_vid = $email->vid;
        unset($email->vid);
      }

      // If this is a new email...
      if ($email->is_new) {
        // Save the new email.
        drupal_write_record('email', $email);

        // Save the initial revision.
        $this->saveRevision($email, $user->uid);

        $op = 'insert';
      } else {
        // Save the updated email.
        drupal_write_record('email', $email, 'eid');

        // If a new email revision was requested, save a new record for that;
        // otherwise, update the email revision record that matches the value
        // of $email->vid.
        if (!empty($email->revision)) {
          $this->saveRevision($email, $user->uid);
        } else {
          $this->saveRevision($email, $user->uid, TRUE);
          $update_email = FALSE;
        }

        $op = 'update';
      }

      // If the revision ID is new or updated, save it to the email.
      if ($update_email) {
        db_update('email')
          ->fields(array('vid' => $email->vid))
          ->condition('eid', $email->eid)
          ->execute();
      }

      // Save fields.
      $function = 'field_attach_' . $op;
      $function('email', $email);

      module_invoke_all('entity_' . $op, $email, 'email');

      // Clear internal properties.
      unset($email->is_new);

      // Ignore slave server temporarily to give time for the saved order to be
      // propagated to the slave.
      db_ignore_slave();

      return $email;
    } catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('email', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Saves an email revision with the uid of the current user.
   *
   * @param $email
   *   The fully loaded email object.
   * @param $uid
   *   The user's uid for the current revision.
   * @param $update
   *   TRUE or FALSE indicating whether or not the existing revision should be
   *     updated instead of a new one created.
   */
  function saveRevision($email, $uid, $update = FALSE) {
    // Hold on to the email's original creator_uid but swap in the revision's
    // creator_uid for the momentary write.
    $temp_uid = $email->uid;
    $email->uid = $uid;

    // Update the existing revision if specified.
    if ($update) {
      drupal_write_record('email_revision', $email, 'vid');
    } else {
      // Otherwise insert a new revision. This will automatically update $email
      // to include the vid.
      drupal_write_record('email_revision', $email);
    }

    // Reset the order's creator_uid to the original value.
    $email->uid = $temp_uid;
  }

  /**
   * Deletes multiple emails by ID.
   *
   * @param $eids
   *   An array of email IDs to delete.
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($eids) {
    if (!empty($eids)) {
      $emails = $this->load($eids, array());

      $transaction = db_transaction();

      try {
        db_delete('email')
          ->condition('eid', $eids, 'IN')
          ->execute();

        db_delete('email_revision')
          ->condition('eid', $eids, 'IN')
          ->execute();

        foreach ($emails as $email_id => $email) {
          field_attach_delete('email', $email);
        }

        // Ignore slave server temporarily to give time for the
        // saved email to be propagated to the slave.
        db_ignore_slave();
      } catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('email', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $email, 'email');

      // Clear the page and block and email caches.
      cache_clear_all();
      $this->resetCache();
    }

    return TRUE;
  }

  /**
   * Create a default email.
   *
   * @param $type
   *   The machine-readable type of the email.
   *
   * @return
   *   An email object with all default fields initialized.
   */
  public function create($type = '') {
    return (object) array(
        'eid' => '',
        'type' => $type,
        'title' => '',
    );
  }
}
