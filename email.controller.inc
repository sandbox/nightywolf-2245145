<?php

class EmailController extends DrupalDefaultEntityController {

  function email_load($eid = NULL, $vid = NULL, $reset = FALSE) {
    $eids = (isset($eid) ? array($eid) : array());
    $conditions = (isset($vid) ? array('vid' => $vid) : array());
    $email = email_load_multiple($eids, $conditions, $reset);
    return $email ? reset($email) : FALSE;
  }

  function email_load_multiple($eids = array(), $conditions = array(), $reset = FALSE) {
    return entity_load('email', $eids, $conditions, $reset);
  }

  public function create($type = '') {
    return (object) array(
        'eid' => '',
        'type' => $type,
        'title' => '',
    );
  }
}
